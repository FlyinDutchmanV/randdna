#include <string>

using std::string;

string randDNA(int seed, string bases, int n)
{
	std::mt19937 eng(seed);			//Random generator to accept provided seed
	string seq;						//Declare string to hold DNA sequence variable
	
	if (bases.size()==0)			//Perform check for blank variable
	{
		seq = "";					//Assign blank variable for randomisation
		return seq;
	}
		
	
	int max = bases.size();			//Set max to the number of bases entered by the user
	std::uniform_int_distribution <int> gen (0, max-1); 	//Index starts at 0; set index number to 1 less than max
		
	for (int i = 0; i < n ; i++)	//Repeat until executed for the number of bases 'int n'
		seq += bases[gen(eng)];		//Pass the randomised result from the string (of letter bases)
	
	return seq;				//DNA sequence returned as string
}
